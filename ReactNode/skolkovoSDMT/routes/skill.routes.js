const {Router} = require('express')
const auth = require('../middleware/auth.middleware')
const db = require("../models")
const Skill = db.skill

const router = Router()


// route prefix = /api/skill

// create project
router.post('/create', auth, async (req, res) => {
        try {
            const {  projectId, name, jiraName } = req.body

            const skill = await Skill.create({ projectId, name, jiraName })

            return res.status(201).json({skill, message: 'Skill matched' })

        } catch (e) {
            res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
        }
    })

// update project by id
router.put('/:id', auth, async (req, res) => {
    try {
        const { projectId, name, jiraName } = req.body

        const skill = await Skill.findByPk(req.params.id)

        if (skill) {
            await skill.update({projectId, name, jiraName})
        }

        return res.status(202).json({ skill, message: 'Skill updated' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

// delete project by id
router.delete('/:id', auth, async (req, res) => {
    try {
        const skill = await Skill.findByPk(req.params.id)

        await skill.destroy()

        return res.status(202).json({ message: 'Skill deleted' })
    } catch (e) {
        res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
    }
})

module.exports = router