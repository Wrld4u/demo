const {Router} = require('express')
const auth = require('../middleware/auth.middleware')
const db = require("../models")
// const User = db.user

const Op = db.Sequelize.Op

const router = Router()


// route prefix = /api/search

// get all releases
// router.post('/', auth, async (req, res) => {
//     try {
//         const { searchStr } = req.body
//
//         // console.log('searchStr', searchStr)
//
//         let releases = await Rel.findAll({ where: {name: {[Op.like]: `%${searchStr}%`}}, include: [
//                 {
//                     model: Cycle,
//                     include: [
//                         {
//                             model: Case,
//                             include: [{
//                                 model: Label,
//                                 attributes: ['id', 'label']
//                             }]
//                         }, Exec
//                     ]
//                 }
//             ] })
//
//         let cases = await Case.findAll({ where: {title: {[Op.like]: `%${searchStr}%`}}, include: [{
//                 model: Label,
//                 attributes: ['id', 'label']
//             },
//                 {
//                     model: Cycle,
//                 },
//                 {
//                     model: Exec,
//                 },
//             ] })
//
//         let cycles = await Cycle.findAll({ where: {name: {[Op.like]: `%${searchStr}%`}}, include: [{
//             model: Case,
//             include: [{
//                 model: Label,
//                 attributes: ['id', 'label']
//             }]
//         },
//             {
//                 model: Exec
//             },
//             {
//                 model: Rel
//             },
//         ] })
//
//         // console.log(releases, cases)
//
//         res.json({ releases, cases, cycles, message: 'ok' })
//     } catch (e) {
//         res.status(500).json({message: `Что-то пошло не так! Ошибка: ${e.message}`})
//     }
// })


module.exports = router