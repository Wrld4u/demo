// Запуск этого приложения: pm2 start start.config.js

module.exports = {
    apps : [{
        name: "workBot",
        script: "./app.js",
        env: {
            NODE_ENV: "production",
        },
        env_dev: {
            NODE_ENV: "production",
        },
        // instances : "2",
        // exec_mode : "cluster"
    }]
}
