import React, {useState, useContext} from 'react'
import {Icon} from "../partials/Icon"
import {useHistory} from 'react-router-dom'
import {SidebarContext} from "../../context/SidebarContext"

export const FlowSidebar = ({project}) => {
    const sideBar = useContext(SidebarContext)
    const [action, setAction] = useState('Action')
    const [filtered, setFiltered] = useState(project && project.blocks.length ? project.blocks.filter(el => el.type === 'Action' ) : [])
    const history = useHistory()
    // const [searching, setSearching] = useState(false)

    const onDragStart = (event, nodeType, b) => {
        event.dataTransfer.setData('application/reactflow', nodeType)
        event.dataTransfer.setData('b', JSON.stringify(b))
        event.dataTransfer.effectAllowed = 'move'
    }

    // console.log('SbPrj', project)
    
    return (
        <aside style={{zIndex: 1000}}>
            <p id="header" className="py-0">Blocks</p>

            <div id="search" className="pt-1">
                <div className="input-field ml-0 pl-0 my-0 text-gray">
                    <i className="fa fa-search prefix" aria-hidden="true" style={{width: '24px', height: '15px', fontSize: 'inherit', marginTop: '5px'}}/>
                    <input
                        placeholder="Search"
                        type="text"
                        id="blck-search"
                        className=""
                        style={{marginLeft: '24px', width: 'calc(100% - 30px)', borderBottom: 'none'}}
                        name="search"
                        autoComplete="off"
                        onKeyUp={(key) => {
                            // setSearching(true)
                            key.target.value
                                ? setFiltered(project && project.blocks.length ? project.blocks.filter(el => el.name.toLowerCase().includes(key.target.value.toLowerCase()) || el.description.toLowerCase().includes(key.target.value.toLowerCase())) : [])
                                : setFiltered(project && project.blocks.length ? project.blocks : [])
                        }}
                    />
                </div>
            </div>

            <div id="subnav" className="d-flex justify-between align-center">
                <div id="actions" className={`side d-flex justify-center ${action === 'Action' ? 'navactive' : 'navdisabled'}`} onClick={() => {
                    setAction('Action')
                    // setSearching(false)
                    setFiltered(project && project.blocks.length ? project.blocks.filter(el => el.type === 'Action' ) : [])
                    document.getElementById('blck-search').value = ''
                }}>
                    <div className="d-flex justify-center align-center">
                        <Icon name='flash' size='20px' mt='2px'/>
                        <span>Actions</span>
                    </div>
                </div>
                <div id="loggers" className={`side d-flex justify-center ${action === 'Block' ? 'navactive' : 'navdisabled'}`} onClick={() => {
                    setAction('Block')
                    // setSearching(false)
                    setFiltered(project && project.blocks.length ? project.blocks.filter(el => el.type === 'Block' ) : [])
                    document.getElementById('blck-search').value = ''
                }}>
                    <div className="d-flex justify-center align-center">
                        <Icon name='box' size='20px' mt='2px'/>
                        <span>Blocks</span>
                    </div>
                </div>
            </div>

            <div id="blocklist">
                {filtered && filtered.length ? filtered.map(b => { return (
                    <React.Fragment key={b.id}>
                        <div className="blockelem create-flowy noselect dndnode d-flex justify-start" onDragStart={(event) => onDragStart(event, 'special', b)} draggable>
                            <div className="grabme">
                                {b.type === 'Action' ? <Icon name='flash' size='20px' mt='-6px'/> : <Icon name='box' size='20px' mt='-6px'/>}
                            </div>
                            <div className="blockin d-flex flex-column">
                                <div className="blocktext">
                                    <p className="blocktitle">{b.name}</p>
                                    <p className="blockdesc">{b.description}</p>
                                </div>
                            </div>
                        </div>
                    </React.Fragment>)
                }) : <></>}
            </div>

            <div id="footer" className="d-flex justify-center align-center">
                <div className='addBtn txt-gray' onClick={() => {
                    history.push(`/project/${project.id}/blocks`)
                    sideBar.toggle(false, project.id)
                    sideBar.toggleMenu('block')
                }}>
                    + add block
                </div>
            </div>
        </aside>
    )
}