import React, {useCallback, useContext, useEffect, useState} from 'react'
import {Tab} from "../partials/Tab"
import {Tag} from "../partials/Tag"
import {Link} from "react-router-dom"
import {useHttp} from "../../hooks/http.hook"
import {AuthContext} from "../../context/AuthContext"
import {Skill} from "../partials/Skill"

export const Block = ({
                          epicId=+new Date(),
                          epic={},
                          project={},
                          css={},
                          onDelete=(eid) => {console.log('EpicId', eid)}
                      }) => {

    const [tags, setTags] = useState([])
    const {loading, request, error, clearError} = useHttp()
    const { token, logout } = useContext(AuthContext)

    useEffect(() => {
        let allTags = []
        epic.tasks.forEach(t => {
            allTags = [...allTags, ...t.labels]
        })

        // console.log('allTags', allTags)
        allTags = [...new Set(allTags)]

        // here match project skills to tags
        // and check with team skills
        allTags = allTags.map(t => {
            return project.skills.find(s => s.jiraName === t) ? project.skills.find(s => s.jiraName === t) : {id: 0, name: `!!${t}!!`, color: 'red'}
        })

        // setTags([...new Set(allTags)])
        setTags(allTags)
    }, [epic, project])

    const deleteEpicHandler = useCallback(async (eId) => {
        const sk = await request(`/api/jira/deleteEpic`, 'POST', {
            jiraName: project.jiraName,
            jiraUrl: project.jiraUrl,
            jiraPass: project.jiraPass,
            epicId: eId
        }, {authorization: 'Bearer ' + token})
        onDelete(eId)
    }, [request])
    
    useEffect(() => {
    }, [tags])

    return (
        <>
            <div className='blockWrapper d-flex justify-between'>
                <div className="row clear-row w-100">
                    <div className="col s8 epic">
                        <table>
                            <tbody className='clearTable'>
                                <tr>
                                    <td colSpan='2'>
                                        <h5 className='mt-0 py-0 mb-0'>
                                            {epic.summary} <span className='txt-gray'>- {epic.status.name}</span>
                                            <i
                                                style={{cursor: 'pointer'}}
                                                className="fa fa-trash-o pl-1 opacity-0"
                                                aria-hidden="true"
                                                onClick={async () => {deleteEpicHandler(epic.id)}}
                                            />
                                        </h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{width: '24px', fontSize: '20px', verticalAlign: 'top'}}><i className="fa fa-hdd-o txt-lightGray" aria-hidden="true" /></td>
                                    <td align="left" className='pl-0 txt-gray'>
                                        {epic.tasks.length ?
                                            `${epic.tasks.filter(ts => ts.status.name === 'Готово').length} / ${epic.tasks.length} задач готово (${Math.floor(epic.tasks.filter(ts => ts.status.name === 'Готово').length * 100 / epic.tasks.length)}%)` :
                                            `В Jira нет связанных задач для данного этапа, необходимо добавить задачи.`
                                        }
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{width: '24px', fontSize: '20px', verticalAlign: 'top'}}><i className="fa fa-hdd-o txt-lightGray" aria-hidden="true" /></td>
                                    <td align="left" className='pl-0 txt-gray'>{tags.length ? tags.map((t, idx) => {return(
                                        <Tag key={idx} name={t.name} style={
                                            project.teams.length && project.teams.filter(te => te.teamUserSkills.length && te.teamUserSkills.filter(ts => ts.skill.name === t.name).length).length ? 'green' : 'red'
                                        } ml='0' mr='10px'/>
                                    )}) : `Нет связанных навыков`}</td>
                                </tr>
                                <tr>
                                    <td style={{width: '24px', fontSize: '20px', verticalAlign: 'top'}}><i className="fa fa-clock-o txt-lightGray" aria-hidden="true" /></td>
                                    <td align="left" className='pl-0 txt-gray'>
                                        {epic.aggregateprogress.total > 0 ?
                                            <span>
                                                Прогнозируемое время выполнения <br/>
                                                <span style={{fontSize: '24px'}}>{Math.ceil(epic.aggregateprogress.total / 3600)} часов</span>
                                                {epic.aggregateprogress.percent ? <span> (Прогресс: {epic.aggregateprogress.percent}%)</span> : <></>}
                                            </span>:
                                            `Примерная оценка срока появится после заполнения данных о команде`
                                        }
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{width: '24px', fontSize: '20px', verticalAlign: 'top'}}><i className="fa fa-user-circle-o txt-lightGray" aria-hidden="true" /></td>
                                    <td align="left" className='pl-0 txt-gray'>При необходимости вы можете сформировать задачу на найм необходимых кандидатов</td>
                                </tr>
                                <tr>
                                    <td style={{width: '24px', fontSize: '20px', verticalAlign: 'top'}}><i className="fa fa-user-circle-o txt-lightGray opacity-0" aria-hidden="true" /></td>
                                    <td align="left" className='pl-0 txt-gray' style={{fontWeight: 400, textDecoration: 'underline'}}><Link className='txt-gray' to='#'>Поставить задачу в HR</Link></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div className="col s4 team">
                        <p>Команда <i className="fa fa-users txt-lightGray" aria-hidden="true" /></p>
                        {project.teams.length ? project.teams.map(el => {return(
                            <React.Fragment key={el.id}>
                                <span>
                                    {el.name ? el.user.name : el.user.email}
                                    {el.teamUserSkills.length ?
                                        <>
                                            <Skill name={el.teamUserSkills[0].skill.name} level={el.teamUserSkills[0].level} showDel={false}/>
                                            {el.teamUserSkills.length > 1 ? <Tag name={`+${el.teamUserSkills.length - 1}`} style='gray'/> : <></>}
                                        </> : <></>}
                                </span>
                            </React.Fragment>
                        )}) : <span>Нет команды</span>}
                    </div>
                </div>
            </div>

            {/*<i*/}
                {/*style={{cursor: 'pointer', paddingLeft: '3px'}}*/}
                {/*className="fa fa-trash-o txt-gray clear-minWidth flow-icon"*/}
                {/*aria-hidden="true"*/}
                {/*onClick={()=>{onDelete(epicId)}}*/}
            {/*/>*/}
        </>
    )
}