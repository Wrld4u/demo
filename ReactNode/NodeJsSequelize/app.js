const express = require('express')
const cors = require('cors')
const config = require('config')
// const path = require('path')

const app = express()

app.use(express.json({ extended: true }))
app.use(cors())
app.use('/api/bids', require('./routes/bids.routes'))
app.use('/api/skills', require('./routes/skills.routes'))
app.use('/api/requestCandidates', require('./routes/requestCandidates.routes'))
app.use('/api/candidateSkills', require('./routes/candidateSkills.routes'))
app.use('/api/search', require('./routes/search.routes'))
app.use('/api/tagging', require('./routes/tagging.routes'))
app.use('/api/person', require('./routes/person.routes'))

const db = require("./models")
db.sequelize.sync()

// working test
app.get('/', async (req, res) => {
    res.status(200).json({message: `working good! :)`})
})

const PORT = process.env.PORT || config.get('port') || 5000

async function start() {
    try {
        app.listen(PORT, () => {
            console.log(`Server hsa been started on ${PORT}`)
        })
    } catch (e) {
        console.log(`Server error: ${e.message}`)
        process.exit(1)
    }
}

start()