// Запуск этого приложения: pm2 start ecosystem.config.js

module.exports = {
    apps : [{
        name: "iamAPI",
        script: "./app.js",
        env: {
            NODE_ENV: "production",
            NODE_OPTIONS: "--max-old-space-size=3072"
        },
        env_dev: {
            NODE_ENV: "production",
        },
        instances: "2",
        exec_mode: "cluster",
        watch: ['controllers', 'models', 'routes']
    }]
}
