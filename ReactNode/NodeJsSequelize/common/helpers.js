const convertMS = (milliseconds) => {
    let day, hour, minute, seconds
    seconds = Math.floor(milliseconds / 1000)
    minute = Math.floor(seconds / 60)
    seconds = seconds % 60
    hour = Math.floor(minute / 60)
    minute = minute % 60
    day = Math.floor(hour / 24)
    hour = hour % 24
    return {
        day: day,
        hour: hour,
        minute: minute,
        seconds: seconds
    }
}

const delay = ms => new Promise(resolve => setTimeout(resolve, ms))

const div = (val, by) => {
    return (val - val % by) / by
}

const formatDate = (date) => {
    let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

/**
 * Вычитание месяца
 * let date = new Date(Date.now())
 * let newDate = new Date(date.setMonth(date.getMonth()-1))
 *
 * @type {{convertMS: function(*), delay: function(*=): Promise<any>, div: function(*, *), formatDate: function(*=)}}
 */

module.exports = {convertMS, delay, div, formatDate}