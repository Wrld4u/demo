const {Router} = require('express')
const candSkill = require('../controllers/candidateSkill.controller')

const router = Router()

// route prefix = /api/candidateSkills

// get all, paginate
router.get('/', candSkill.getAll)

// get one
router.get('/:id', candSkill.getOne)

// Create
router.post('/', candSkill.createCS)

// Update
router.put('/:id', candSkill.updateCS)

// Delete
router.delete('/:id', candSkill.deleteCS)


module.exports = router