const {Router} = require('express')
const bids = require('../controllers/bid.search.controller')

const router = Router()

// route prefix = /api/search

//get bid with candidates
router.get('/', bids.getBidwCand)

module.exports = router