const {Router} = require('express')
const recCands = require('../controllers/requestCandidate.controller')

const router = Router()

// route prefix = /api/requestCandidates

// get all, paginate
router.get('/', recCands.getAll)

// get one
router.get('/:id', recCands.getOne)

// Create
router.post('/', recCands.createRC)

// Update
router.put('/:id', recCands.updateRC)

// Delete
router.delete('/:id', recCands.deleteRC)


module.exports = router