const {Router} = require('express')
const bids = require('../controllers/bid.controller')

const router = Router()

// route prefix = /api/bid

// get all, paginate
router.get('/', bids.getAll)

// get one
router.get('/:id', bids.getOne)

// Create
router.post('/', bids.createBid)

// Update
router.put('/:id', bids.updateBid)

// Delete
router.delete('/:id', bids.deleteBid)


module.exports = router