const {Router} = require('express')
const person = require('../controllers/person.controller')

const router = Router()

// route prefix = /api/person

// get all, paginate
// router.get('/', bids.getAll)

// get one
// router.get('/:id', bids.getOne)

// set lastContact
router.post('/lastContact', person.setLastContact)

// set ids
router.post('/ids', person.setIds)

// city + age
router.post('/person', person.cityAge)

// phone
router.post('/phone', person.phone)

// mail
router.post('/mail', person.mail)

// name
router.post('/name', person.name)

// mainTech
router.post('/mainTech', person.mainTech)

// gradeLevel
router.post('/gradeLevel', person.gradeLevel)

// assumedGrade
router.post('/assumedGrade', person.assumedGrade)

// EXPERIENCE
// Create
router.post('/experience', person.addExp)
//Delete
router.delete('/experience/:id', person.delExp)
// Update
router.put('/experience/:id', person.changeExp)

// EDUCATION
// Create
router.post('/education', person.addEdu)
//Delete
router.delete('/education/:id', person.delEdu)
// Update
router.put('/education/:id', person.changeEdu)

// LANGUAGE
// Create
router.post('/lang', person.addLang)
//Delete
router.delete('/lang/:id', person.delLang)
// Update
router.put('/lang/:id', person.changeLang)

// SKILLS
// Create
router.post('/skill', person.addSkill)
//Delete
router.delete('/skill/:id', person.delSkill)
// Update
router.put('/skill/:id', person.changeSkill)

// get persons in period (created)
router.post('/inPeriod', person.inPeriod)

// create new person with all tables
router.post('/createPerson', person.createNew)


module.exports = router