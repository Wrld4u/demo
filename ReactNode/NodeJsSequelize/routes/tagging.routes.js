const {Router} = require('express')
const tag = require('../controllers/tag.controller')

const router = Router()

// route prefix = /api/tagging

//start tagging persons by skills
router.get('/', tag.tagAll)

router.get('/one/:id', tag.tagOne)

//for testing not used
router.post('/t', tag.tagTest)

module.exports = router