const {Router} = require('express')
const skills = require('../controllers/skill.controller')

const router = Router()

// route prefix = /api/skills

//get All, paginate
router.get('/', skills.getAll)

// Create
router.post('/', skills.createS)

// Update
router.put('/:id', skills.updateS)

// Delete
router.delete('/:id', skills.deleteS)

module.exports = router