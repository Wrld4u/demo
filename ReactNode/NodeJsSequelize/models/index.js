const config = require('config')
const Sequelize = require('sequelize')

const dbConfig = config.get('DBConn')

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    // operatorsAliases: false,
    logging: dbConfig.logging,

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    },

    dialectOptions: {
        options: {
            requestTimeout: 50000
        }
    },

})

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.bid = require('./bid.model')(sequelize, Sequelize)
db.skill = require('./skill.model')(sequelize, Sequelize)
db.requestCandidate = require('./requestCandidate.model')(sequelize, Sequelize)
db.candidateSkill = require('./candidateSkill.model')(sequelize, Sequelize)
db.requestSkill = require('./requestSkill.model')(sequelize, Sequelize)

db.person = require('./person.model')(sequelize, Sequelize)
db.pSkill = require('./person.skill.model')(sequelize, Sequelize)
db.pPos = require('./person.position.model')(sequelize, Sequelize)
db.pTest = require('./person.test.model')(sequelize, Sequelize)
db.pEdu = require('./person.edu.model')(sequelize, Sequelize)
db.pCourse = require('./person.course.model')(sequelize, Sequelize)
db.pAbout = require('./person.about.model')(sequelize, Sequelize)
db.pExp = require('./person.exp.model')(sequelize, Sequelize)
db.pIds = require('./person.ids.model')(sequelize, Sequelize)
db.pLang = require('./person.lang.model')(sequelize, Sequelize)
db.pCitizen = require('./person.citizen.model')(sequelize, Sequelize)
db.pContact = require('./person.contact.model')(sequelize, Sequelize)
db.pDrvcat = require('./person.drvcat.model')(sequelize, Sequelize)
db.pMetro = require('./person.metro.model')(sequelize, Sequelize)
db.pPermit = require('./person.permit.model')(sequelize, Sequelize)
db.pRecomm = require('./person.recomm.model')(sequelize, Sequelize)
db.pReloc = require('./person.relocate.model')(sequelize, Sequelize)

module.exports = db