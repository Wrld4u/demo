const db = require("../models")
const log = require('simple-node-logger').createSimpleFileLogger('./logs/bids.log')
const Bid = db.bid
const Skill = db.skill
const ReqSkill = db.requestSkill
const Op = db.Sequelize.Op

// Bid.hasMany(ReqSkill, {foreignKey: 'request_id'})
// ReqSkill.belongsTo(Bid, { foreignKey: 'request_id' })
Bid.belongsToMany(Skill, {through: ReqSkill, foreignKey: 'request_id'})
Skill.belongsToMany(Bid, { through: ReqSkill, foreignKey: 'skill_id' })

const div = (val, by) => {
    return (val - val % by) / by;
}

/**
 *
 * @param rSkills
 * @returns {Promise<[any , any , any , any , any , any , any , any , any , any]>}
 * "skills_id": [
    {
         "id": 1,
         "name": "JavaScript",
         "isRequired": true,
         "score": 2
    }]
 */
const prepare_iSkills = async (rSkills) => {
    try {
        let iSkills = await Skill.findAll({attributes: ['id', 'title', 'synonyms']})
        iSkills = iSkills.map(el => { // skills from i_skills
            el.dataValues.total = [el.dataValues.title.toLowerCase()]
            if (el.dataValues.synonyms) {
                el.dataValues.total = [...el.dataValues.total, ...el.dataValues.synonyms.toLowerCase().split(',').map(s => s.trim())]
            }
            return el.dataValues
        })

        let skills_id = await Promise.all(rSkills.map(async skill => { //skill = name of skill or synonym

            const isk = iSkills.find(el => el.total.includes(skill.name.toLowerCase()))

            if (isk) {
                return {id: isk.id, ...skill}
            } else {
                const sk = await Skill.create({title: skill.name, synonyms: null})
                return {id: sk.id, ...skill}
            }

        }))

        return skills_id

    } catch (e) {
        console.log(`preparing skills ${e}`)
        log.error(`preparing skills ${e}`)
        return []

    }
}

// Retrieve all bids with paginate.
const getAll = async (req, res) => {
    let page = Number(req.query.page) || 0

    try {
        const total = await Bid.count()
        const pages = div(total, 100)

        // const bids = await Bid.findAll({ offset: page * 100, limit: 100, include: [ReqSkill] })
        const bids = await Bid.findAll({ offset: page * 100, limit: 100, include: [Skill] })

        res.status(200).json({ total, page, pages, bids })
    } catch (e) {
        res.status(500).json({message: `Error getting requests ${e.message}`})
    }
}

// get One by id
const getOne = async (req, res) => {
    const id = req.params.id

    try {
        const bid = await Bid.findOne( {where: {id}, include: [Skill]} )

        res.status(200).json({ bid })
    } catch (e) {
        res.status(500).json({message: `Error getting request by id ${e.message}`})
    }
}

// Create
const createBid = async (req, res) => {
    try {
        const {
            name, company_id, status, competitors, requirements, functionality, conditions, salary,
            bonus_from, bonus_to, benefits, employment_type, adress_office, nearest_metro, age_from,
            age_to, citizenship, gender, workday_start, workday_end, profile_education, skills
        } = req.body

        // if (skills.length) {
        //     try {
        //         const skills_id = await prepare_iSkills(skills)
        //
        //         res.status(201).json({skills_id})
        //     } catch (e) {
        //         console.log(e)
        //         res.status(500).json({message: `error ${e}`})
        //     }
        //
        // }

        // transaction: create bid, check all skills in table i_skills and match request <-> skills in i_request_skills
        try {
            const result = await db.sequelize.transaction(async (t) => {

                const bid = await Bid.create({ name, company_id, status, competitors, requirements, functionality, conditions, salary,
                    bonus_from, bonus_to, benefits, employment_type, adress_office, nearest_metro, age_from,
                    age_to, citizenship, gender, workday_start, workday_end, profile_education }, { transaction: t })

                if (skills.length) {
                    const skills_id = await prepare_iSkills(skills)

                    await Promise.all(skills_id.map(async skId => {
                        await ReqSkill.findOrCreate({where: {request_id: bid.id, skill_id: skId.id, is_required: skId.isRequired, score: skId.score}, transaction: t})
                    }))
                }

                return bid;

            })

            let bid = await Bid.findOne({
                where: {
                    id: result.id
                },
                include: [Skill]
            })

            res.status(201).json({ bid })
        } catch (e) {
            log.error(`Can't create request with skills: ${e}`)
            console.log(`Can't create request with skills: ${e}`)
            res.status(500).json({message: `Error creating request id: ${e.message}`})
        }


    } catch (e) {
        res.status(500).json({message: `Error creating request id: ${e.message}`})
    }
}

// Update
const updateBid = async (req, res) => {
    const id = req.params.id
    try {
        const {
            name, company_id, status, competitors, requirements, functionality, conditions, salary,
            bonus_from, bonus_to, benefits, employment_type, adress_office, nearest_metro, age_from,
            age_to, citizenship, gender, workday_start, workday_end, profile_education, skills
        } = req.body

        let bid = await Bid.findByPk( id )

        if (bid) {
            await bid.update({name, company_id, status, competitors, requirements, functionality, conditions, salary,
                bonus_from, bonus_to, benefits, employment_type, adress_office, nearest_metro, age_from,
                age_to, citizenship, gender, workday_start, workday_end, profile_education})

            if (skills.length) {
                const skills_id = await prepare_iSkills(skills)

                await Promise.all(skills_id.map(async skId => {
                    await ReqSkill.findOrCreate({where: {request_id: bid.id, skill_id: skId.id, is_required: skId.isRequired, score: skId.score}})
                }))
            }

            bid = await Bid.findOne({
                where: {
                    id: bid.id
                },
                include: [Skill]
            })

            res.status(202).json({ bid })
        }

        res.status(404).json({message: `There no request with id: ${id}`})

    } catch (e) {
        res.status(500).json({message: `Error updating request ${e.message}`})
    }
}

// Delete
const deleteBid = async (req, res) => {
    const id = req.params.id
    try {
        // { where: { id: [1,2,3,4] }}
        const reqSkills = await ReqSkill.destroy({ where: {request_id: id}})
        let bid = await Bid.destroy({ where: {id}})
        res.status(202).json({ message: `Deleted request ${bid}` })
    } catch (e) {
        res.status(500).json({message: `Error deleting request ${e.message}`})
    }
}

module.exports = { getAll, getOne, createBid, deleteBid, updateBid }