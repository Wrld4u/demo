const db = require("../models")
const log = require('simple-node-logger').createSimpleFileLogger('./logs/tag.log')
const fs = require('fs')
const iSkill = db.skill
const iCandSkill = db.candidateSkill

const Person = db.person
const PSkill = db.pSkill
const PPos = db.pPos
const PTest = db.pTest
const PEdu = db.pEdu
const PCourse = db.pCourse
const PAbout = db.pAbout
const PExp = db.pExp
const PIds = db.pIds

const Op = db.Sequelize.Op

const div = (val, by) => {
    return (val - val % by) / by;
}

const getSkills = async (persId) => {
    try {
        const skills = await PSkill.findAll({ where: {owner_id: persId}, attributes: ['name'] })
        const positions = await PPos.findAll({ where: {owner_id: persId}, attributes: ['func'] })
        const tests = await PTest.findAll({ where: {owner_id: persId}, attributes: ['name'] })
        const education = await PEdu.findAll({ where: {owner_id: persId}, attributes: ['spec'] })
        const courses = await PCourse.findAll({ where: {owner_id: persId}, attributes: ['spec'] })
        const abouts = await PAbout.findAll({ where: {owner_id: persId}, attributes: ['text'] })
        const experiences = await PExp.findAll({ where: {owner_id: persId}, attributes: ['position', 'description'] })

        let searchArr = []
        searchArr.push(skills.map(el => el.dataValues.name).join(' '))
        searchArr.push(positions.map(el => el.dataValues.func).join(' '))
        searchArr.push(tests.map(el => el.dataValues.name).join(' '))
        searchArr.push(education.map(el => el.dataValues.spec).join(' '))
        searchArr.push(courses.map(el => el.dataValues.spec).join(' '))
        searchArr.push(abouts.map(el => el.dataValues.text).join(' '))
        searchArr.push(experiences.map(el => `${el.dataValues.position} ${el.dataValues.description}`).join(' '))

        return searchArr.join(' ')
    } catch (e) {
        console.log(e)
        log.error(`Error in getSkills ${e}`)
    }

    return ''
}

const rmRegSpecial = function (el) {
    return el.dataValues.synonyms.toLowerCase().split(',').map(ss => `(${ss.trim()
        .replace('*', '\\*')
        .replace('[', '\\[')
        .replace('$', '\\$')
        .replace('.', '\\.')
        .replace('?', '\\?')
        .replace('(', '\\(')
        .replace(')', '\\)')
        .replace('^', '\\^')
        .replace('|', '\\|')
        .replace('+', '\\+')})`)
}

/**
 * - возраст кандидата меньше 24
 - или зарплатные ожидания от 9000 до 120.000
 - или если кандидат родился после (born) 1996 года (т е если 1990 - not match).
 *
 */
const tagMatched = async function (person) {
    try {
        if (person) {
            let Matched = true

            if (person.age && Number(person.age) < 24) {
                Matched = false
                // log.info(`age: ${Matched}`)
            }
            if (person.salary_sum && (Number(person.salary_sum) > 9000 && Number(person.salary_sum) < 120000)) {
                Matched = false
                // log.info(`salary_sum: ${Matched}`)
            }

            if (person.born && new Date(person.born.toString()) > new Date('1996-12-31')) {
                Matched = false
                // log.info(`born: ${Matched}`)
            }

            if (!Matched) {
                person.Matched = false
                await person.save()
            }
            return person
        }
    } catch (e) {
        console.log(e)
        log.error(`Can't set Matched: ${person.id} - ${person.name}`)
    }
}

const tagAll = async (req, res) => {
    let start = Number(req.query.start) || 0

    res.status(200).json({ message: `Tagging persons in progress...` })

    try {
        const total = await Person.count()
        const limit = 5
        const chunks = div(total, limit)

        const iSkills = await iSkill.findAll({ attributes: ['id', 'title', 'synonyms'] })

        try {
            fs.truncate('./logs/tag.log', 0, () => {console.log('log file cleared')})
        } catch (e) {
            console.log(e)
        }

        log.info(`total chunks ${chunks}`)
        // this run chunk promises one by one (waiting chunk finished
        for (let chunk = start; chunk <= chunks; chunk++) {

            let persons = await Person.findAll({ offset: chunk * limit, limit})

            persons = await Promise.all(persons.map(async person => {
                log.info(`chank.person ${chunk}.${person.id} - ${person.name}`)

                // tag Matched here
                person = await tagMatched(person)

                let ss = await getSkills(person.id)
                if (person.position) {
                    ss += person.position
                }

                // check skills
                await Promise.all(iSkills.map(async el => {
                    let saveIt = false

                    if (ss.toLowerCase().includes(el.dataValues.title.toLowerCase())) {
                        saveIt = true
                    }

                    if (el.dataValues.synonyms) {
                        let substrings = rmRegSpecial(el)

                        if (substrings.length && new RegExp(substrings.join("|")).test(ss.toLowerCase())) {
                            // At least one match
                            saveIt = true
                        }
                    }

                    if (saveIt) {
                        // log.warn(`save iSkill`)
                        try {
                            // todo: collect all skills and bulk write it
                            // find or create iCandSkill
                            await iCandSkill.findOrCreate({where: {candidate_id: person.id, skill_id: el.dataValues.id}})
                        } catch (e) {
                            console.log(e)
                            log.error(`save skill err: ${e} [chank.person ${chunk}.${person.id} - ${person.name}]`)
                        }
                    }

                }))
                // check skills END

                // person.dataValues.skillStr = ss
                // return person.dataValues
            }))

        }

    } catch (e) {
        log.error(`Error on tagging: ${e}`)
        // res.status(500).json({message: `Error getting requests ${e.message}`})
    }
}

const tagOne = async (req, res) => {
    const id = req.params.id

    try {
        const iSkills = await iSkill.findAll({ attributes: ['id', 'title', 'synonyms'] })

        let person = await Person.findByPk(id)

        // tag Matched here
        person = await tagMatched(person)

        let ss = await getSkills(id)
        if (person.position) {
            ss += person.position
        }

        // check skills
        await Promise.all(iSkills.map(async el => {
            let saveIt = false

            if (ss.toLowerCase().includes(el.dataValues.title.toLowerCase())) {
                saveIt = true
            }

            if (el.dataValues.synonyms) {
                let substrings = rmRegSpecial(el)

                if (substrings.length && new RegExp(substrings.join("|")).test(ss.toLowerCase())) {
                    // At least one match
                    saveIt = true
                }
            }

            if (saveIt) {
                // log.warn(`save iSkill`)
                try {
                    // find or create iCandSkill
                    await iCandSkill.findOrCreate({where: {candidate_id: person.id, skill_id: el.dataValues.id}})
                } catch (e) {
                    console.log(e)
                    log.error(`------------> save skill err: ${e} [person ${person.id} - ${person.name}]`)
                }
            }

        }))

        log.info(`person ok: ${person.id} - ${person.name}`)
        res.status(200).json({ message: `Tagging persons finished` })

        // check skills END
        // res.status(200).json({ message: `Tagging persons finished...`, person })


    } catch (e) {
        log.error(`Error on tagging: ${e}`)
        // res.status(500).json({message: `Error getting requests ${e.message}`})
    }
}

// for testing not used
const _tagTest = async (req, res) => {
    let id = Number(req.query.id) || 0
    try {
        Person.hasMany(PSkill, {foreignKey: 'owner_id'})
        Person.hasMany(PPos, {foreignKey: 'owner_id'})
        Person.hasMany(PTest, {foreignKey: 'owner_id'})
        Person.hasMany(PEdu, {foreignKey: 'owner_id'})
        Person.hasMany(PCourse, {foreignKey: 'owner_id'})
        Person.hasMany(PAbout, {foreignKey: 'owner_id'})
        Person.hasMany(PExp, {foreignKey: 'owner_id'})

        const item = await Person.findAll({ where: {id}, include: [
                { model: PSkill, attributes: ['name'] },
                { model: PPos, attributes: ['func'] },
                { model: PTest, attributes: ['name'] },
                { model: PEdu, attributes: ['spec'] },
                { model: PCourse, attributes: ['spec'] },
                { model: PAbout, attributes: ['text'] },
                { model: PExp, attributes: ['position'] },
            ] })

        res.status(200).json({ item })

        // res.status(200).json({ message: 'ok' })
        // console.log('okokokook')

    } catch (e) {
        res.status(500).json({message: `Error getting skills ${e.message}`})
    }
}

const tagTest = async (req, res) => {
    // let page = Number(req.query.page) || 0
    // let woMt = req.query.without_main_tech || false
    const { page = 0, specs = [], withoutMT = false } = req.body

    // woMt ? null : {}

    // specs = ['JS', 'JavaScript', 'Java-Script', 'Frontend', 'Фронтенд', 'Программист', 'React', 'Angular', 'Vue', 'Reactjs', 'Vuejs', 'Angularjs', 'Бэкенд', 'Бекенд', 'Backend', 'Back-end', 'Developer', 'Разработчик', 'Java', 'C#', 'C++', 'Python', 'PHP', 'Ruby', 'QA', 'DevOps', 'Dart', 'Flutter', 'Swift', 'Xcode', 'Objective-C', 'IOS', 'Kotlin', 'Django', 'Flask', 'RXJava', 'Unity', 'Unreal Engine', '1C', '1С', 'Abap', 'Go', 'Golang', 'Engineer', 'Scala', 'Fullstack', 'Фулстак', 'Full-Stack', 'Delphi', 'Laravel', 'Symphony', 'Тестировщик', 'Тестированию', 'Testing', 'Quality Assurance', 'ML', 'AI', 'NLP', 'Data Science', 'Big Data']

    try {
        Person.hasMany(PExp, {foreignKey: 'owner_id'})
        Person.hasMany(PIds, {foreignKey: 'owner_id'})

        // let abouts = await db.sequelize.query(`Select distinct owner_id from about where text in (${specs.map(el => `'${el}'`)})`, {type: db.sequelize.QueryTypes.SELECT})
        // abouts = abouts && abouts.length ? abouts.map(el => el.owner_id) : []
        //
        // let courses = await db.sequelize.query(`Select distinct owner_id from course where spec in (${specs.map(el => `'${el}'`)}) or name in (${specs.map(el => `'${el}'`)})`, {type: db.sequelize.QueryTypes.SELECT})
        // courses = courses && courses.length ? courses.map(el => el.owner_id) : []
        //
        // let educations = await db.sequelize.query(`Select distinct owner_id from education where spec in (${specs.map(el => `'${el}'`)}) or name in (${specs.map(el => `'${el}'`)})`, {type: db.sequelize.QueryTypes.SELECT})
        // educations = educations && educations.length ? educations.map(el => el.owner_id) : []
        //
        // let experienses = await db.sequelize.query(`Select distinct owner_id from experience where position in (${specs.map(el => `'${el}'`)}) or description in (${specs.map(el => `'${el}'`)})`, {type: db.sequelize.QueryTypes.SELECT})
        // experienses = experienses && experienses.length ? experienses.map(el => el.owner_id) : []
        //
        // let persons = await db.sequelize.query(`Select distinct id from person where position in (${specs.map(el => `'${el}'`)}) or section in (${specs.map(el => `'${el}'`)})`, {type: db.sequelize.QueryTypes.SELECT})
        // persons = persons && persons.length ? persons.map(el => el.id) : []
        //
        // let positions = await db.sequelize.query(`Select distinct owner_id from position where func in (${specs.map(el => `'${el}'`)})`, {type: db.sequelize.QueryTypes.SELECT})
        // positions = positions && positions.length ? positions.map(el => el.owner_id) : []
        //
        // let skills = await db.sequelize.query(`Select distinct owner_id from skill where name in (${specs.map(el => `'${el}'`)})`, {type: db.sequelize.QueryTypes.SELECT})
        // skills = skills && skills.length ? skills.map(el => el.owner_id) : []
        //
        // let tests = await db.sequelize.query(`Select distinct owner_id from test where spec in (${specs.map(el => `'${el}'`)}) or name in (${specs.map(el => `'${el}'`)})`, {type: db.sequelize.QueryTypes.SELECT})
        // tests = tests && tests.length ? tests.map(el => el.owner_id) : []

        // let about = db.sequelize.query(`Select distinct owner_id from about where text in (${specs.map(el => `'${el}'`)})`, {type: db.sequelize.QueryTypes.SELECT})
        // let course = db.sequelize.query(`Select distinct owner_id from course where spec in (${specs.map(el => `'${el}'`)}) or name in (${specs.map(el => `'${el}'`)})`, {type: db.sequelize.QueryTypes.SELECT})
        // let education = db.sequelize.query(`Select distinct owner_id from education where spec in (${specs.map(el => `'${el}'`)}) or name in (${specs.map(el => `'${el}'`)})`, {type: db.sequelize.QueryTypes.SELECT})
        // let experiense = db.sequelize.query(`Select distinct owner_id from experience where position in (${specs.map(el => `'${el}'`)}) or description in (${specs.map(el => `'${el}'`)})`, {type: db.sequelize.QueryTypes.SELECT})
        // let person = db.sequelize.query(`Select distinct id from person where position in (${specs.map(el => `'${el}'`)}) or section in (${specs.map(el => `'${el}'`)})`, {type: db.sequelize.QueryTypes.SELECT})
        // let position = db.sequelize.query(`Select distinct owner_id from position where func in (${specs.map(el => `'${el}'`)})`, {type: db.sequelize.QueryTypes.SELECT})
        // let skill = db.sequelize.query(`Select distinct owner_id from skill where name in (${specs.map(el => `'${el}'`)})`, {type: db.sequelize.QueryTypes.SELECT})
        // let test = db.sequelize.query(`Select distinct owner_id from test where spec in (${specs.map(el => `'${el}'`)}) or name in (${specs.map(el => `'${el}'`)})`, {type: db.sequelize.QueryTypes.SELECT})

        let about = db.sequelize.query(`Select distinct owner_id from about where ${specs.map(el => `text like '%${el}%'`).join(' or ')}`, {type: db.sequelize.QueryTypes.SELECT})
        let course = db.sequelize.query(`Select distinct owner_id from course where ${specs.map(el => `spec like '%${el}%'`).join(' or ')} or ${specs.map(el => `name like '%${el}%'`).join(' or ')}`, {type: db.sequelize.QueryTypes.SELECT})
        let education = db.sequelize.query(`Select distinct owner_id from education where ${specs.map(el => `spec like '%${el}%'`).join(' or ')} or ${specs.map(el => `name like '%${el}%'`).join(' or ')}`, {type: db.sequelize.QueryTypes.SELECT})
        let experiense = db.sequelize.query(`Select distinct owner_id from experience where ${specs.map(el => `position like '%${el}%'`).join(' or ')} or ${specs.map(el => `description like '%${el}%'`).join(' or ')}`, {type: db.sequelize.QueryTypes.SELECT})
        let person = db.sequelize.query(`Select distinct id from person where ${specs.map(el => `position like '%${el}%'`).join(' or ')} or ${specs.map(el => `section like '%${el}%'`).join(' or ')}`, {type: db.sequelize.QueryTypes.SELECT})
        let position = db.sequelize.query(`Select distinct owner_id from position where ${specs.map(el => `func like '%${el}%'`).join(' or ')}`, {type: db.sequelize.QueryTypes.SELECT})
        let skill = db.sequelize.query(`Select distinct owner_id from skill where ${specs.map(el => `name like '%${el}%'`).join(' or ')}`, {type: db.sequelize.QueryTypes.SELECT})
        let test = db.sequelize.query(`Select distinct owner_id from test where ${specs.map(el => `spec like '%${el}%'`).join(' or ')} or ${specs.map(el => `name like '%${el}%'`).join(' or ')}`, {type: db.sequelize.QueryTypes.SELECT})

        let [abouts, courses, educations, experienses, persons, positions, skills, tests] = await Promise.all([about, course, education, experiense, person, position, skill, test])

        abouts = abouts && abouts.length ? abouts.map(el => el.owner_id) : []
        courses = courses && courses.length ? courses.map(el => el.owner_id) : []
        educations = educations && educations.length ? educations.map(el => el.owner_id) : []
        experienses = experienses && experienses.length ? experienses.map(el => el.owner_id) : []
        persons = persons && persons.length ? persons.map(el => el.id) : []
        positions = positions && positions.length ? positions.map(el => el.owner_id) : []
        skills = skills && skills.length ? skills.map(el => el.owner_id) : []
        tests = tests && tests.length ? tests.map(el => el.owner_id) : []

        let all = [...abouts, ...courses, ...educations, ...experienses, ...persons, ...positions, ...skills, ...tests]
        all = [...new Set(all)]

        const pages = div(all.length, 50)

        const whereObj = !withoutMT ? {id: {[Op.in]: all}} : {id: {[Op.in]: all}, mainTechnology: null}

        const items = await Person.findAll( { offset: page * 50, limit: 50, where: whereObj, attributes: ['id', 'name', 'age', 'phone', 'email', 'salary_sum', 'updated_at', 'Matched', 'lastContact', 'mainTechnology', 'gradeLevel', 'assumedGrade'],
            include: [
                {
                    model: PExp,
                    attributes: ['start', 'end', 'company', 'city', 'position', 'description'],
                },
                {
                    model: PIds,
                    attributes: ['id_cv'],
                }
            ]
        } )

        res.status(200).json({ total: all.length, page, pages, items })
        // res.status(200).json({ t })

        // res.status(200).json({ message: 'ok' })
        // console.log('okokokook')

    } catch (e) {
        res.status(500).json({message: `Error getting skills ${e.message}`})
    }
}

module.exports = { tagAll, tagTest, tagOne }