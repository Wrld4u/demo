const db = require("../models")
const log = require('simple-node-logger').createSimpleFileLogger('./logs/search.log')
const Bid = db.bid
const Person = db.person
const ReqSkill = db.requestSkill
const ReqCand = db.requestCandidate
const CandSkill = db.candidateSkill
const Op = db.Sequelize.Op

Bid.hasMany(ReqSkill, {foreignKey: 'request_id'})
ReqSkill.belongsTo(Bid, { foreignKey: 'request_id' })

Bid.hasMany(ReqCand, {foreignKey: 'request_id'})
ReqCand.belongsTo(Bid, { foreignKey: 'request_id' })

Person.hasMany(CandSkill, { foreignKey: 'candidate_id' })
CandSkill.belongsTo(Person, { foreignKey: 'candidate_id' })

//find candidates matches to bid, fill request_candidates and return bid with candidates
const getBidwCand = async (req, res) => {
    let id = req.query.bid || null
    let isUpdate = req.query.update || null

    res.status(200).json({ message: 'in work' })

    try {
        // get bid with skills
        let bid = await Bid.findOne( {where: {id}, include: [ReqSkill, ReqCand]} )
        // calculate bid skills multiply
        const reqSkillsIds = bid.dataValues.i_request_skills.map(el => el.skill_id)
        const skillsMult = bid.dataValues.i_request_skills.reduce((acc, el) => acc * el.score, 1)

        // res.status(200).json({ reqSkillsIds, skillsMult, bid })

        // check if no request_candidates || isUpdate
        // find Candidates in person and write to request_candidates
        if (!bid.i_request_candidates.length || isUpdate) {
            // find candidates
            // const persons = Person.findAll({where: {'CandSkills.skill_id': {$in: reqSkillsIds}}, include: [CandSkill]})
            let persons = await Person.findAll({include: [{
                    model: CandSkill,
                    where: {
                        skill_id: {[Op.in]: reqSkillsIds}
                    }
                }]})

            // Фильтрация по з/п
            // if (bid.salary) {
            //     const sPrcnt = 35
            //     const prcntSum = bid.dataValues.salary/100*sPrcnt
            //     const minS = bid.dataValues.salary - prcntSum
            //     const maxS = bid.dataValues.salary + prcntSum
            //
            //     persons = persons.filter(pers => {
            //         if (pers.salary_sum) {
            //             if (bid.dataValues.salary >= minS && bid.dataValues.salary <= maxS) {
            //                 return true
            //             }
            //         }
            //         return false
            //     })
            // }

            // const cPrcnt = 40
            // const cPrcntSum = skillsMult/100*cPrcnt
            // const minM = skillsMult - cPrcntSum
            // const maxM = skillsMult + cPrcntSum
            //
            // persons = persons.filter(pers => {
            //     const skillCMult = pers.i_candidate_skills.reduce((acc, el) => acc * el.score, 1)
            //     // console.log(`${pers.name} - ${skillCMult} / ${skillsMult}`)
            //     if (skillCMult >= minM && skillCMult <= maxM) {
            //         return true
            //     } else {
            //         return false
            //     }
            // })

            // res.status(200).json({ skIds: reqSkillsIds, persons })

            // write to request candidates
            await Promise.all(persons.map(async pers => {
                await ReqCand.findOrCreate({where: {request_id: bid.id, candidate_id: pers.id}, defaults: {status: 'in progress', mult: pers.i_candidate_skills.reduce((acc, el) => acc * el.score, 1)}})
            }))
        }

        // get bid with ReqCand
        // bid = await Bid.findOne( {where: {id}, include: [ReqSkill, ReqCand]} )

        // return finded candidates
        // res.status(200).json({ bid })

        log.info('Finished bid: ' + bid.id )
    } catch (e) {
        res.status(500).json({message: `Error getting requests ${e.message}`})
    }
}

module.exports = { getBidwCand }