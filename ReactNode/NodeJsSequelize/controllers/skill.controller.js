const db = require("../models")
const Skill = db.skill
const Op = db.Sequelize.Op

const div = (val, by) => {
    return (val - val % by) / by;
}

// Retrieve all skills with paginate.
const getAll = async (req, res) => {
    let page = Number(req.query.page) || 0
    let text = req.query.text || null

    try {
        if (!text) {
            const total = await Skill.count()
            const pages = div(total, 100)

            const skills = await Skill.findAll({ offset: page * 100, limit: 100 })

            res.status(200).json({ total, page, pages, skills })
        }

        const skills = await Skill.findAll({ where: {
                [Op.or]: [{title: {[Op.substring]: text}}, {synonyms: {[Op.substring]: text}}]
            } })

        res.status(200).json({ skills })

    } catch (e) {
        res.status(500).json({message: `Error getting skills ${e.message}`})
    }
}

// Create
const createS = async (req, res) => {
    try {
        const { title, synonyms } = req.body

        const item = await Skill.create({ title, synonyms })

        res.status(201).json({ item })

    } catch (e) {
        res.status(500).json({message: `Error creating Skill id: ${e.message}`})
    }
}

// Update
const updateS = async (req, res) => {
    const id = req.params.id
    try {
        const { title, synonyms } = req.body

        let item = await Skill.findByPk( id )

        if (item) {
            await item.update({ title, synonyms })

            res.status(202).json({ item })
        }

        res.status(404).json({message: `There no Skill with id: ${id}`})

    } catch (e) {
        res.status(500).json({message: `Error updating Skill ${e.message}`})
    }
}

// Delete
const deleteS = async (req, res) => {
    const id = req.params.id
    try {
        let item = await Skill.destroy({ where: {id}})
        res.status(202).json({ message: `Deleted Skill ${item}` })
    } catch (e) {
        res.status(500).json({message: `Error deleting Skill ${e.message}`})
    }
}

module.exports = {getAll, createS, updateS, deleteS}