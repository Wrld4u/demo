const db = require("../models")
const CandS = db.candidateSkill
const PIds = db.pIds
const Skill = db.skill
const Op = db.Sequelize.Op

CandS.belongsTo(Skill, {foreignKey: 'skill_id'})
Skill.hasMany(CandS, { foreignKey: 'skill_id' })

const div = (val, by) => {
    return (val - val % by) / by;
}

// Retrieve all with paginate.
const getAll = async (req, res) => {
    let page = Number(req.query.page) || 0
    let candID = Number(req.query.id) || 0
    let candId_cv = Number(req.query.idcv) || 0

    try {

        if (candId_cv !== 0) {
            let ids = await PIds.findOne({where: {id_cv: candId_cv}})
            candID = ids.owner_id
        }

        const total = await CandS.count({where: {candidate_id: candID}})
        const pages = div(total, 100)

        const items = await CandS.findAll({where: {candidate_id: candID}, offset: page * 100, limit: 100,
                include: [
                    {
                        model: Skill,
                        attributes: ['title', 'synonyms']
                    }
                    ]
        })

        res.status(200).json({ total, page, pages, items })
    } catch (e) {
        res.status(500).json({message: `Error getting candidateSkills ${e.message}`})
    }
}

// get One by id
const getOne = async (req, res) => {
    const id = req.params.id

    try {
        // here add sort by multy of candidates skills
        const item = await CandS.findOne( {where: {id: id}} )

        res.status(200).json({ item })
    } catch (e) {
        res.status(500).json({message: `Error getting candidateSkill by id ${e.message}`})
    }
}

// Create
const createCS = async (req, res) => {
    try {
        let { skill_id, candidate_id = 0, score, id_cv } = req.body

        if (id_cv) {
            let ids = await PIds.findOne({where: {id_cv}})
            candidate_id = ids.owner_id
        }

        const item = await CandS.create({ skill_id, candidate_id, score })

        res.status(201).json({ item })

    } catch (e) {
        res.status(500).json({message: `Error creating candidateSkill id: ${e.message}`})
    }
}

// Update
const updateCS = async (req, res) => {
    const id = req.params.id
    try {
        const { skill_id, candidate_id, score } = req.body

        let item = await CandS.findByPk( id )

        if (item) {
            await item.update({ skill_id, candidate_id, score })

            res.status(202).json({ item })
        }

        res.status(404).json({message: `There no candidateSkill with id: ${id}`})

    } catch (e) {
        res.status(500).json({message: `Error updating candidateSkill ${e.message}`})
    }
}

// Delete
const deleteCS = async (req, res) => {
    const id = req.params.id
    try {
        let item = await CandS.findByPk( id )
        await item.destroy()
        // let item = await CandS.destroy({ where: {id}})
        res.status(202).json({ message: `Deleted candidateSkill ${item.id}` })
    } catch (e) {
        res.status(500).json({message: `Error deleting candidateSkill ${e.message}`})
    }
}

module.exports = { getAll, createCS, updateCS, deleteCS, getOne }