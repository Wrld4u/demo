const db = require("../models")
const log = require('simple-node-logger').createSimpleFileLogger('./logs/tag.log')
const fs = require('fs')

const Person = db.person
const About = db.pAbout
const PIds = db.pIds
const Exp = db.pExp
const Edu = db.pEdu
const Skill = db.pSkill
const Lang = db.pLang
const Citizen = db.pCitizen
const Contact = db.pContact
const Course = db.pCourse
const Drvcat = db.pDrvcat
const Metro = db.pMetro
const Permit = db.pPermit
const Pos = db.pPos
const Recomm = db.pRecomm
const Relocate = db.pReloc
const Test = db.pTest

const Op = db.Sequelize.Op

const div = (val, by) => {
    return (val - val % by) / by;
}

Person.hasMany(Exp, {foreignKey: 'owner_id'})
Person.hasMany(PIds, {foreignKey: 'owner_id'})

// Сделать АПИ запрос, с помощью которого можно будет проставлять Last Contacted
const setLastContact = async (req, res) => {
    try {
        const { personId, lastContact } = req.body

        let person = await Person.findByPk( personId )

        if (person) {
            person.lastContact = lastContact
            await person.save()
        }

        res.status(202).json({ person })
    } catch (e) {
        console.log(e)
        res.status(500).json({message: `Error updating lastContact ${e.message}`})
    }
}

// Проставка id_cv, id_job по номеру телефона/email
const setIds = async (req, res) => {
    try {
        const { phone, id_cv, id_job, email } = req.body

        let person = await Person.findOne({where: {phone: {[Op.like]: `%${phone}%`}}})

        if (person) {
            await PIds.findOrCreate({where: {owner_id: person.id, id_cv, id_job}})
        } else {
            person = await Person.findOne({where: {email: {[Op.like]: `%${email}%`}}})
            if (person) {
                await PIds.findOrCreate({where: {owner_id: person.id, id_cv, id_job}})
            }
        }

        res.status(202).json({ person })
    } catch (e) {
        console.log(e)
        res.status(500).json({message: `Error set ids by phone/email ${e.message}`})
    }
}

// change city, age of person
const cityAge = async (req, res) => {
    try {
        const { id_cv, city = null, age = null } = req.body
        let id = null

        let ids = await PIds.findOne({where: {id_cv}})
        id = ids.owner_id

        let person = await Person.findByPk(id)
        if (person) {
            if (city) {
                person.addr = city
            }
            if (age) {
                person.age = age
            }

            person.save()
        }

        res.status(202).json({ person })
    } catch (e) {
        console.log(e)
        res.status(500).json({message: `Error set city, age by id_cv ${e.message}`})
    }
}

// change phone of person
const phone = async (req, res) => {
    try {
        const { id_cv, phone = null } = req.body
        let id = null

        let ids = await PIds.findOne({where: {id_cv}})
        id = ids.owner_id

        let person = await Person.findByPk(id)
        if (person) {
            if (phone) {
                person.phone = phone
            }

            person.save()
        }

        res.status(202).json({ person })
    } catch (e) {
        console.log(e)
        res.status(500).json({message: `Error set phone by id_cv ${e.message}`})
    }
}

// change mail of person
const mail = async (req, res) => {
    try {
        const { id_cv, mail = null } = req.body
        let id = null

        let ids = await PIds.findOne({where: {id_cv}})
        id = ids.owner_id

        let person = await Person.findByPk(id)
        if (person) {
            if (mail) {
                person.email = mail
            }

            person.save()
        }

        res.status(202).json({ person })
    } catch (e) {
        console.log(e)
        res.status(500).json({message: `Error set mail by id_cv ${e.message}`})
    }
}

// change name of person
const name = async (req, res) => {
    try {
        const { id_cv, name = null } = req.body
        let id = null

        let ids = await PIds.findOne({where: {id_cv}})
        id = ids.owner_id

        let person = await Person.findByPk(id)
        if (person) {
            if (name) {
                person.name = name
            }

            person.save()
        }

        res.status(202).json({ person })
    } catch (e) {
        console.log(e)
        res.status(500).json({message: `Error set name by id_cv ${e.message}`})
    }
}

// change mainTechnology of person
const mainTech = async (req, res) => {
    try {
        const { id_cv, mainTech = null } = req.body
        let id = null

        let ids = await PIds.findOne({where: {id_cv}})
        id = ids.owner_id

        let person = await Person.findByPk(id)
        if (person) {
            person.mainTechnology = mainTech
            person.save()
        }

        res.status(202).json({ person })
    } catch (e) {
        console.log(e)
        res.status(500).json({message: `Error set main technology by id_cv ${e.message}`})
    }
}

// change gradeLevel of person
const gradeLevel = async (req, res) => {
    try {
        const { id_cv, grade = null } = req.body
        let id = null

        let ids = await PIds.findOne({where: {id_cv}})
        id = ids.owner_id

        let person = await Person.findByPk(id)
        if (person) {
            person.gradeLevel = grade
            person.save()
        }

        res.status(202).json({ person })
    } catch (e) {
        console.log(e)
        res.status(500).json({message: `Error set grade level by id_cv ${e.message}`})
    }
}

// change gradeLevel of person
const assumedGrade = async (req, res) => {
    try {
        const { id_cv, assumedGrade = null } = req.body
        let id = null

        let ids = await PIds.findOne({where: {id_cv}})
        id = ids.owner_id

        let person = await Person.findByPk(id)
        if (person) {
            person.assumedGrade = assumedGrade
            person.save()
        }

        res.status(202).json({ person })
    } catch (e) {
        console.log(e)
        res.status(500).json({message: `Error set assumed grade level by id_cv ${e.message}`})
    }
}

// EXPERIENCE
// add Experience for person
const addExp = async (req, res) => {
    try {
        let { id_cv, start = '01.01.3000', end = '01.01.3000', company = '', city = '', site = null, position = '', description = '' } = req.body

        let ids = await PIds.findOne({where: {id_cv}})
        let id = ids.owner_id

        const item = await Exp.create({ owner_id: id, start, end, company, city, site, position, description })

        res.status(201).json({ item })

    } catch (e) {
        res.status(500).json({message: `Error creating Experience: ${e.message}`})
    }
}

// change Experience for person
const changeExp = async (req, res) => {
    const id = req.params.id
    try {
        let { start, end, company, city, site, position, description } = req.body

        let item = await Exp.findByPk( id )

        if (item) {
            await item.update({ start, end, company, city, site, position, description })

            res.status(202).json({ item })
        }

        res.status(404).json({message: `There no Experience with id: ${id}`})

    } catch (e) {
        res.status(500).json({message: `Error updating Experience ${e.message}`})
    }
}

// delete Experience for person
const delExp = async (req, res) => {
    const id = req.params.id
    try {
        let item = await Exp.destroy({ where: {id}})
        res.status(202).json({ message: `Deleted Experience ${item}` })
    } catch (e) {
        res.status(500).json({message: `Error deleting Experience ${e.message}`})
    }
}

// EDUCATION
// add Education for person
const addEdu = async (req, res) => {
    try {
        let { id_cv, year = '01.01.3000', name = '', spec = '' } = req.body

        let ids = await PIds.findOne({where: {id_cv}})
        let id = ids.owner_id

        const item = await Edu.create({ owner_id: id, year, name, spec })

        res.status(201).json({ item })

    } catch (e) {
        res.status(500).json({message: `Error creating Education: ${e.message}`})
    }
}

// change Education for person
const changeEdu = async (req, res) => {
    const id = req.params.id
    try {
        let { year, name, spec } = req.body

        let item = await Edu.findByPk( id )

        if (item) {
            await item.update({ year, name, spec })

            res.status(202).json({ item })
        }

        res.status(404).json({message: `There no Education with id: ${id}`})

    } catch (e) {
        res.status(500).json({message: `Error updating Education ${e.message}`})
    }
}

// delete Education for person
const delEdu = async (req, res) => {
    const id = req.params.id
    try {
        let item = await Edu.destroy({ where: {id}})
        res.status(202).json({ message: `Deleted Education ${item}` })
    } catch (e) {
        res.status(500).json({message: `Error deleting Education ${e.message}`})
    }
}

// LANGUAGE
// add Language for person
const addLang = async (req, res) => {
    try {
        let { id_cv, name = '', value = '' } = req.body

        let ids = await PIds.findOne({where: {id_cv}})
        let id = ids.owner_id

        const item = await Lang.create({ owner_id: id, name, value })

        res.status(201).json({ item })

    } catch (e) {
        res.status(500).json({message: `Error creating Language: ${e.message}`})
    }
}

// change Language for person
const changeLang = async (req, res) => {
    const id = req.params.id
    try {
        let { name, value } = req.body

        let item = await Lang.findByPk( id )

        if (item) {
            await item.update({ name, value })

            res.status(202).json({ item })
        }

        res.status(404).json({message: `There no Language with id: ${id}`})

    } catch (e) {
        res.status(500).json({message: `Error updating Language ${e.message}`})
    }
}

// delete Language for person
const delLang = async (req, res) => {
    const id = req.params.id
    try {
        let item = await Lang.destroy({ where: {id}})
        res.status(202).json({ message: `Deleted Language ${item}` })
    } catch (e) {
        res.status(500).json({message: `Error deleting Language ${e.message}`})
    }
}

// SKILL
// add Skill for person
const addSkill = async (req, res) => {
    try {
        let { id_cv, name = '' } = req.body

        let ids = await PIds.findOne({where: {id_cv}})
        let id = ids.owner_id

        const item = await Skill.create({ owner_id: id, name })

        res.status(201).json({ item })

    } catch (e) {
        res.status(500).json({message: `Error creating Skill: ${e.message}`})
    }
}

// change Language for person
const changeSkill = async (req, res) => {
    const id = req.params.id
    try {
        let { name } = req.body

        let item = await Skill.findByPk( id )

        if (item) {
            await item.update({ name })

            res.status(202).json({ item })
        }

        res.status(404).json({message: `There no Skill with id: ${id}`})

    } catch (e) {
        res.status(500).json({message: `Error updating Skill ${e.message}`})
    }
}

// delete Language for person
const delSkill = async (req, res) => {
    const id = req.params.id
    try {
        let item = await Skill.destroy({ where: {id}})
        res.status(202).json({ message: `Deleted Skill ${item}` })
    } catch (e) {
        res.status(500).json({message: `Error deleting Skill ${e.message}`})
    }
}

//
const inPeriod = async (req, res) => {
    try {
        const { start, end = '01.01.3000' } = req.body

        let persons = await Person.findAll({
            where:
                {
                    created_at: {[Op.and]: {[Op.gte]: start, [Op.lt]: end}},
                },
            include: [{
                model: Exp,
                attributes: ['start', 'end', 'company', 'city', 'position'],
            }, {
                model: PIds,
                attributes: ['id_cv'],
            }]
        })


        res.status(200).json({ persons })
    } catch (e) {
        console.log(e)
        res.status(500).json({message: `Error getting persons in period ${e.message}`})
    }
}

// create new person with all tables
const createNew = async (req, res) => {

    const fillLinked = async (Model, data, obj) => {
        for (let i=0; i<data.length; i++) {
            await Model.create({...obj, ...data[i]})
        }
    }

    let data = {}

    try {
        const {
            person,
            about=[],
            citizen=[],
            contact=[],
            course=[],
            drvcat=[],
            education=[],
            experience=[],
            ids,
            lang=[],
            metro=[],
            permit=[],
            position=[],
            recomm=[],
            relocate=[],
            skill=[],
            test=[],
        } = req.body

        let pers = await Person.findOne( {where: {email: person.email}} )
        // let pers = await Person.create( {...person} )

        if (!pers) {
            pers = await Person.create( {...person} )

            await PIds.create({owner_id: pers.id, id_cv: ids.id_cv, id_job: ids.id_job})

            if (about.length) {
                await fillLinked(About, about, {owner_id: pers.id})
            }
            if (citizen.length) {
                await fillLinked(Citizen, citizen, {owner_id: pers.id})
            }
            if (contact.length) {
                await fillLinked(Contact, contact, {owner_id: pers.id})
            }
            if (course.length) {
                await fillLinked(Course, course, {owner_id: pers.id})
            }
            if (drvcat.length) {
                await fillLinked(Drvcat, drvcat, {owner_id: pers.id})
            }
            if (education.length) {
                await fillLinked(Edu, education, {owner_id: pers.id})
            }
            if (experience.length) {
                await fillLinked(Exp, experience, {owner_id: pers.id})
            }
            if (lang.length) {
                await fillLinked(Lang, lang, {owner_id: pers.id})
            }
            if (metro.length) {
                await fillLinked(Metro, metro, {owner_id: pers.id})
            }
            if (permit.length) {
                await fillLinked(Permit, permit, {owner_id: pers.id})
            }
            if (position.length) {
                await fillLinked(Pos, position, {owner_id: pers.id})
            }
            if (recomm.length) {
                await fillLinked(Recomm, recomm, {owner_id: pers.id})
            }
            if (relocate.length) {
                await fillLinked(Relocate, relocate, {owner_id: pers.id})
            }
            if (skill.length) {
                await fillLinked(Skill, skill, {owner_id: pers.id})
            }
            if (test.length) {
                await fillLinked(Test, test, {owner_id: pers.id})
            }

            let idcv = await PIds.findAll({where: {owner_id: pers.id}})
            data = {
                Exist_in_base: 'no',
                person_id: pers.id,
                id_cv: idcv.map(el => el.id_cv)
            }
        } else
            {
                let idcv = await PIds.findAll({where: {owner_id: pers.id}})
                data = {
                    Exist_in_base: 'yes',
                    person_id: pers.id,
                    id_cv: idcv.map(el => el.id_cv)
                }
            }

        res.status(202).json({ data })
    } catch (e) {
        console.log(e)
        res.status(500).json({message: `Error ${e.message}`})
    }

}


module.exports = {
    setLastContact, setIds, cityAge, addExp, changeExp, delExp, inPeriod, phone, mail, name, addEdu, changeEdu,
    delEdu, addLang, changeLang, delLang, addSkill, changeSkill, delSkill, mainTech, gradeLevel, assumedGrade,
    createNew
}