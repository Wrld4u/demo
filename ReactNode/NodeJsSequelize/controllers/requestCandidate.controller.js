const db = require("../models")
const {formatDate} = require('../common/helpers')
const ReqCand = db.requestCandidate
// const CandSkill = db.candidateSkill
const Person = db.person
const PExp = db.pExp
const PIds = db.pIds
const Op = db.Sequelize.Op

const div = (val, by) => {
    return (val - val % by) / by;
}

ReqCand.belongsTo(Person, {foreignKey: 'candidate_id'})
Person.hasMany(PExp, {foreignKey: 'owner_id'})
Person.hasMany(PIds, {foreignKey: 'owner_id'})
// Person.hasMany(ReqCand, {foreignKey: 'candidate_id'})

// Person.hasMany(CandSkill, {foreignKey: 'candidate_id'})

// Retrieve all with paginate.
const getAll = async (req, res) => {
    let page = Number(req.query.page) || 0
    let id = Number(req.query.id) || 0
    let skill = Number(req.query.skill) || 0

    // let skillWhere = {}
    // skill ? skillWhere = {skill_id: {[Op.in]: skill}} : {}
    // skill ? skillWhere = {skill_id: skill} : {}

    let date = new Date(Date.now())
    let newDate = new Date(date.setMonth(date.getMonth()-1))
    date = formatDate(newDate)

    if (!id) {
        try {
            const total = await ReqCand.count()
            const pages = div(total, 10)

            const items = await ReqCand.findAll({ offset: page * 10, limit: 10,
                where: {
                    mult: {[Op.gt]: 4},
                }, include: [{
                    model: Person,
                    where: {
                        Matched: true,
                        lastContact: {
                            [Op.or]: {[Op.is]: null, [Op.lt]: date}
                        }
                    },
                    attributes: ['id', 'name', 'age', 'phone', 'email', 'salary_sum', 'updated_at', 'Matched', 'lastContact'],
                    include: [{
                        model: PExp,
                        attributes: ['start', 'end', 'company', 'city', 'position'],
                    }, {
                        model: PIds,
                        attributes: ['id_cv'],
                    }]
                }] })

            res.status(200).json({ total, page, pages, items })
        } catch (e) {
            res.status(500).json({message: `Error getting requestCandidates ${e.message}`})
        }
    } else {
        try {
            const total = await ReqCand.count({where: {request_id: id, mult: {[Op.gt]: 4}}, include: [{
                    model: Person,
                    attributes: ['id', 'name', 'age', 'salary_sum', 'updated_at', 'Matched', 'lastContact'],
                    where: {
                        Matched: true,
                        lastContact: {
                            [Op.or]: {[Op.is]: null, [Op.lt]: date}
                        }
                    },
                }]
            })
            const pages = div(total, 50)

            // here add sort by multy of candidates skills
            const items = await ReqCand.findAll( { offset: page * 50, limit: 50, where: {request_id: id, mult: {[Op.gt]: 4}}, order: [['mult', 'DESC']], include: [{
                    model: Person,
                    attributes: ['id', 'name', 'age', 'phone', 'email', 'salary_sum', 'updated_at', 'Matched', 'lastContact'],
                    where: {
                        Matched: true,
                        lastContact: {
                            [Op.or]: {[Op.is]: null, [Op.lt]: date}
                        }
                    },
                    include: [{
                        model: PExp,
                        attributes: ['start', 'end', 'company', 'city', 'position'],
                    }, {
                        model: PIds,
                        attributes: ['id_cv'],
                    }//, {
                        // model: CandSkill,
                        // skillWhere,
                        // attributes: ['skill_id'],
                    ]//}]
                }] } )

            res.status(200).json({ total, page, pages, items })
        } catch (e) {
            res.status(500).json({message: `Error getting requestCandidates by id ${e.message}`})
        }
    }

}

// get One by id
const getOne = async (req, res) => {
    const id = req.params.id

    try {
        // here add sort by multy of candidates skills
        const item = await ReqCand.findAll( {where: {request_id: id}, order: [['mult', 'DESC']], include: [{
                model: Person,
                attributes: ['id', 'name', 'age', 'salary_sum', 'updated_at'],
                include: [{
                    model: PExp,
                    attributes: ['start', 'end', 'company', 'city', 'position'],
                }]
            }]} )

        res.status(200).json({ item })
    } catch (e) {
        res.status(500).json({message: `Error getting requestCandidates by id ${e.message}`})
    }
}

// Create
const createRC = async (req, res) => {
    try {
        const { request_id, candidate_id, status } = req.body

        const item = await ReqCand.create({ request_id, candidate_id, status })

        res.status(201).json({ item })

    } catch (e) {
        res.status(500).json({message: `Error creating requestCandidate id: ${e.message}`})
    }
}

// Update
const updateRC = async (req, res) => {
    const id = req.params.id
    try {
        const { request_id, candidate_id, status } = req.body

        let item = await ReqCand.findByPk( id )

        if (item) {
            await item.update({ request_id, candidate_id, status })

            res.status(202).json({ item })
        }

        res.status(404).json({message: `There no requestCandidate with id: ${id}`})

    } catch (e) {
        res.status(500).json({message: `Error updating requestCandidate ${e.message}`})
    }
}

// Delete
const deleteRC = async (req, res) => {
    const id = req.params.id
    try {
        let item = await ReqCand.destroy({ where: {id}})
        res.status(202).json({ message: `Deleted requestCandidate ${item}` })
    } catch (e) {
        res.status(500).json({message: `Error deleting requestCandidate ${e.message}`})
    }
}

module.exports = { getAll, createRC, updateRC, deleteRC, getOne }