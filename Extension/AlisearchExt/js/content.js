var bar = 0;
var turn = {main:{},similar:{}};
var its_results = false;
var in_ali = false;
var date = (new Date()).getTime();

chrome.runtime.sendMessage({pageReady:"done"});

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    if (request.hasOwnProperty('progressBar')) {
        var duration = 1000;
        if (!document.getElementById('progressBar' + chrome.runtime.id)) {
            var progressBar = document.createElement('div');
            progressBar.id = 'progressBar' + chrome.runtime.id;
            progressBar.style = 'position: fixed; top: 0; left: 0;height:10px; z-index: 999999999999999999999999;background: linear-gradient(to right, #05f5ed 0%, #0000ff 100%);';
            document.body.appendChild(progressBar);
        }
        if (request.set > bar) {
            bar = request.set;
            if (request.set <= 10 || request.set >= 85) {
                duration = 500
            } else {
                duration = 250
            }
            $('#progressBar' + chrome.runtime.id).animate({
                width: bar + '%'
            }, duration);
        }
        if (request.set == 100){
            bar = 0;
            $('#progressBar' + chrome.runtime.id).remove();
        }
    }
    if (request.hasOwnProperty('resultForTab')) {
        its_results = true;
        var turn_data = {type: request.turn.split('_')[0], num: request.turn.split('_')[1]};

        var append_this ='<a id="' + request.turn + '" href="' + request.result.url + '" target="_blank">' +
            '<div class="item">'+
            '<img src="' + request.result.images[0] + '" alt = "'+request.result.query+'">'+
            '<div class="salePrice">' + request.result.price_now + '</div>';
        if (request.result.price_old == 'no_promo') {
            append_this += '</div></a>';
        } else {
            append_this +=
                '<div class="originalPrice">' + request.result.price_old + '</div></div></a>';
        }
        turn[turn_data.type][turn_data.num]=append_this;
        turned();
    }
});

function turned() {
    var previous = -1;
    for (var num in turn.main){
        if (turn.main.hasOwnProperty(num)){
            if (!document.getElementById('main_'+num)){
                if (previous != -1){
                    document.getElementById('main_'+previous).insertAdjacentHTML('afterend',turn.main[num])
                } else {
                    document.getElementById('goods').insertAdjacentHTML('afterbegin',turn.main[num])
                }
            }
        }
        previous = num;
    }
    previous = -1;
    for (var num in turn.similar){
        if (turn.similar.hasOwnProperty(num)){
            if (!document.getElementById('similar_'+num)){
                if (previous != -1){
                    document.getElementById('similar_'+previous).insertAdjacentHTML('afterend',turn.similar[num])
                } else {
                    document.getElementById('goodsSimilar').insertAdjacentHTML('afterbegin',turn.similar[num])
                }
            }
        }
        previous = num;
    }
}

chrome.storage.local.get(null, function (info) {
    if ((window.location.hostname).indexOf('ebay') + 1) {
        $(document).ready(function() {
            var shadow_remove = setInterval(function () {
                $('#zoom_selector').remove();
            },100);
        });
    }
    if ((window.location.hostname).indexOf('amazon') + 1) {
        $(document).ready(function() {
            var shadow_remove = setInterval(function () {
                $('#magnifierLens').remove();
            },100);
        });
    }

    if ((window.location.hostname).indexOf('aliexpress.com') + 1) {
        $(document).ready(function() {
            var shadow_remove = setInterval(function () {
                $('.shadow').remove();
                $('.magnifier-cover').remove();
                $('.ui-magnifier-glass').remove();
            },100);
        });
    }
});