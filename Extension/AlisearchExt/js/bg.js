var can_send = true;
var step = 0;
var answer = {main:[],similar:{urls:[],results:[]}};
var xhr = new XMLHttpRequest();
var xhr_result;
var url;
var parser = new DOMParser();
answer.query = '';
var requested = false;
var turn = false;
var navigation_hash = '';
var result_page = {};
var result_page_ready = {};

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    if (request.captcha == "done") {
        can_send = true;
        try {
            chrome.tabs.update(request.from_tab, {
                active: true
            });
            chrome.tabs.remove(request.captcha_tab)
        } catch (e) {
        }
    }
    if (request.checkClosed == "go") {
        try {
            chrome.tabs.get(request.check_this, function (tab) {
                if (chrome.runtime.lastError) {
                    chrome.tabs.get(request.from_tab, function (ch_tab) {
                        if (!chrome.runtime.lastError) {
                            chrome.tabs.update(request.from_tab, {
                                active: true
                            });
                        }
                    });
                }
            });
        } catch (e) {
        }
    }
    if (request.hasOwnProperty('pageReady')){
        if (result_page.hasOwnProperty(sender.tab.id)){
            result_page_ready[sender.tab.id] = true;
        }
    }
});

var options = {
    title: chrome.i18n.getMessage("bgTitle"),
    contexts: ['image'],
    onclick: goSearch
};
chrome.contextMenus.create(options);

function goSearch(info, tab) {
    var turn_check = setInterval(function () {
        if (!turn) {
            turn = true;
            clearInterval(turn_check);
            chrome.tabs.sendMessage(tab.id,{progressBar:"add",set:0});
            parseGooglePages(info.srcUrl, tab);
        }
    }, 500);
}
function base64ImageToBlob(str) {
    var pos = str.indexOf(';base64,');
    var type = str.substring(5, pos);
    var b64 = str.substr(pos + 8);

    var imageContent = atob(b64);

    var buffer = new ArrayBuffer(imageContent.length);
    var view = new Uint8Array(buffer);

    for (var n = 0; n < imageContent.length; n++) {
        view[n] = imageContent.charCodeAt(n);
    }

    return new Blob([buffer], {type: type});
}

function parseGooglePages(imageUrl, tab) {
    step++;
    var ResponseToHTML;
    var formData;
    var lenght_to_needed;
    var first_results;
    requested = false;
    try {
        ResponseToHTML = parser.parseFromString(xhr_result, "text/html");
    } catch (e) {
    }
    if (step == 1) {
        chrome.tabs.sendMessage(tab.id, {progressBar: "add", set: 5});
        if ((imageUrl.indexOf('data:image') + 1) && (imageUrl.indexOf('base64') + 1)) {
            formData = new FormData();
            formData.append("encoded_image", base64ImageToBlob(imageUrl));
            xhr.open('POST', 'https://www.google.com/searchbyimage/upload', true);
            xhr.send(formData);
        }
        else {
            url = "http://www.google.com/searchbyimage?hl=ru&site=imghp&image_url=" + imageUrl;
            xhr.open('GET', url, true);
            xhr.send();
        }
    } else if (step == 2) {
        chrome.tabs.sendMessage(tab.id, {progressBar: "add", set: 10});
        try {
            answer.query = ResponseToHTML.querySelector('a.fKDtNb').innerText;
        } catch (e) {
        }
        if ((imageUrl.indexOf('data:image') + 1) && (imageUrl.indexOf('base64') + 1)) {
            formData = new FormData();
            formData.append("encoded_image", base64ImageToBlob(imageUrl));
            formData.append("q", answer.query + ' site:aliexpress.com');
            xhr.open('POST', 'https://www.google.com/searchbyimage/upload', true);
            xhr.send(formData);
        }
        else {
            url = "http://www.google.com/searchbyimage?q=" + answer.query + " site:aliexpress.com&hl=ru&site=imghp&image_url=" + imageUrl;
            xhr.open('GET', url, true);
            xhr.send();
        }
    } else if (step == 3) {
        try {
            if (ResponseToHTML.querySelector('a.iu-card-header')) {
                answer.similar['urls'].push('https://www.google.com' + ResponseToHTML.querySelector('a.iu-card-header').href.split(ResponseToHTML.querySelector('a.iu-card-header').href.split('//')[1].split('/')[0])[1]);
            }
            lenght_to_needed = ResponseToHTML.querySelector('div#rso').getElementsByClassName('bkWMgd').length - 1;
            first_results = ResponseToHTML.querySelector('div#rso').getElementsByClassName('bkWMgd')[lenght_to_needed].getElementsByClassName('rc');
            for (var i = 0; i < first_results.length; i++) {
                if (first_results[i].getElementsByTagName('img')[0]) {
                    if (first_results[i].getElementsByTagName('a')[0].href.indexOf('com/i') + 1) {
                        var item_num = first_results[i].getElementsByTagName('a')[0].href.split('.htm')[0].split('/')[first_results[i].getElementsByTagName('a')[0].href.split('.htm')[0].split('/').length - 1];
                        if (!(answer['main'].indexOf(item_num) + 1) && ((item_num.trim()) != '')) {
                            answer['main'].push(item_num);
                        }
                    }
                }
            }
            if (ResponseToHTML.getElementById('nav')) {
                var navigation = ResponseToHTML.getElementById('nav').getElementsByTagName('td');
                for (var i = 1; i < navigation.length; i++) {
                    var max_nav = navigation[navigation.length - 2].innerText;
                    var cur_nav = ResponseToHTML.getElementById('nav').getElementsByClassName('cur')[0].innerText;
                    var set_set = cur_nav * 40 / max_nav;
                    if (navigation[i].className == 'cur') {
                        if (navigation[i + 1].className != 'b navend') {
                            step--;
                            chrome.tabs.sendMessage(tab.id, {progressBar: "add", set: 10 + Math.round(set_set)});
                            if (navigation_hash == '') {
                                navigation_hash = 'https://www.google.com' + navigation[i + 1].querySelector('a').href.split(navigation[i + 1].querySelector('a').href.split('//')[1].split('/')[0])[1];
                            }
                            xhr.open('GET', navigation_hash, true);
                            xhr.send();
                            break;
                        }
                    }
                }
            }
        } catch (e) {
        }
        if (step == 3) {
            chrome.tabs.sendMessage(tab.id, {progressBar: "add", set: 50});
            if ((imageUrl.indexOf('data:image') + 1) && (imageUrl.indexOf('base64') + 1)) {
                formData = new FormData();
                formData.append("encoded_image", base64ImageToBlob(imageUrl));
                formData.append("q", 'site:aliexpress.com');
                xhr.open('POST', 'https://www.google.com/searchbyimage/upload', true);
                xhr.send(formData);
            }
            else {
                url = "http://www.google.com/searchbyimage?q=site:aliexpress.com&hl=ru&site=imghp&image_url=" + imageUrl;
                xhr.open('GET', url, true);
                xhr.send();
            }
        }
    } else if (step == 4) {
        try {
            if (ResponseToHTML.querySelector('a.iu-card-header')) {
                answer.similar['urls'].push('https://www.google.com' + ResponseToHTML.querySelector('a.iu-card-header').href.split(ResponseToHTML.querySelector('a.iu-card-header').href.split('//')[1].split('/')[0])[1]);
            }
            lenght_to_needed = ResponseToHTML.querySelector('div#rso').getElementsByClassName('bkWMgd').length - 1;
            first_results = ResponseToHTML.querySelector('div#rso').getElementsByClassName('bkWMgd')[lenght_to_needed].getElementsByClassName('rc');
            for (var i = 0; i < first_results.length; i++) {
                if (first_results[i].getElementsByTagName('img')[0]) {
                    if (first_results[i].getElementsByTagName('a')[0].href.indexOf('com/i') + 1) {
                        var item_num = first_results[i].getElementsByTagName('a')[0].href.split('.htm')[0].split('/')[first_results[i].getElementsByTagName('a')[0].href.split('.htm')[0].split('/').length - 1];
                        if (!(answer['main'].indexOf(item_num) + 1) && ((item_num.trim()) != '')) {
                            answer['main'].push(item_num);
                        }
                    }
                }
            }
            if (ResponseToHTML.getElementById('nav')) {
                var navigation = ResponseToHTML.getElementById('nav').getElementsByTagName('td');
                var max_nav = navigation[navigation.length - 2].innerText;
                var cur_nav = ResponseToHTML.getElementById('nav').getElementsByClassName('cur')[0].innerText;
                var set_set = cur_nav * 49 / max_nav;
                for (var i = 1; i < navigation.length; i++) {
                    if (navigation[i].className == 'cur') {
                        if (navigation[i + 1].className != 'b navend') {
                            step--;
                            chrome.tabs.sendMessage(tab.id, {progressBar: "add", set: 50 + Math.round(set_set)});
                            if (navigation_hash == '') {
                                navigation_hash = 'https://www.google.com' + navigation[i + 1].querySelector('a').href.split(navigation[i + 1].querySelector('a').href.split('//')[1].split('/')[0])[1];
                            }
                            xhr.open('GET', navigation_hash, true);
                            xhr.send();
                            break;
                        }
                    }
                }
            }
        } catch (e) {
        }
        if (step == 4) {
            chrome.tabs.sendMessage(tab.id, {progressBar: "add", set: 95});
            if (answer.similar.urls[0]) {
                xhr.open('GET', answer.similar.urls[0], true);
                xhr.send();
            } else {
                step = 6;
                requested = true;
            }
        }
    } else if (step == 5) {
        chrome.tabs.sendMessage(tab.id, {progressBar: "add", set: 97});

        var similar_res = ResponseToHTML.getElementsByClassName('iKjWAf');
        for (var i = 0; i < similar_res.length; i++) {
            try {
                var href = JSON.parse(similar_res[i].parentElement.getElementsByClassName('rg_meta notranslate')[0].innerText)['ru'];
                href = href.split('/')[href.split('/').length - 1].split('.htm')[0];
                if ((!isNaN(href)) && ((href.trim()) != '') && (!(answer.similar.results.indexOf(href) + 1)) && (!(answer.main.indexOf(href) + 1))) {
                    answer.similar.results.push(href);
                }
            } catch (e) {
            }
        }
        if (answer.similar.urls[1]) {
            xhr.open('GET', answer.similar.urls[1], true);
            xhr.send();
        } else {
            step = 6;
            requested = true;
        }
    } else if (step == 6) {
        chrome.tabs.sendMessage(tab.id, {progressBar: "add", set: 100});

        var similar_res = ResponseToHTML.getElementsByClassName('iKjWAf');
        for (var i = 0; i < similar_res.length; i++) {
            try {
                var href = JSON.parse(similar_res[i].parentElement.getElementsByClassName('rg_meta notranslate')[0].innerText)['ru'];
                href = href.split('/')[href.split('/').length - 1].split('.htm')[0];
                if ((!isNaN(href)) && ((href.trim()) != '') && (!(answer.similar.results.indexOf(href) + 1)) && (!(answer.main.indexOf(href) + 1))) {
                    answer.similar.results.push(href);
                }
            } catch (e) {
            }
        }

        requested = true;
    }
    xhr.onreadystatechange = function () {
        if (this.readyState != 4) {
            return;
        }
        if (this.status != 200) {
            if (this.status == 429) {
                can_send = false;
                var wait_captcha = setInterval(function () {
                    if (can_send) {
                        clearInterval(wait_captcha);
                        if (navigation_hash == '') {
                            step--
                        }
                        if (step < 0) {
                            step++;
                        }
                        requested = true;
                    }
                }, 1000);
                var captcha = parser.parseFromString(this.responseText, "text/html");
                chrome.tabs.create({
                        url: 'https://www.google.com/sorry/index?continue=' + encodeURIComponent(captcha.getElementsByName('continue')[0].value) + '&q=' + captcha.getElementsByName('q')[0].value,
                        active: true
                    },
                    function (cap_tab) {
                        result_page[cap_tab.id] = true;
                        var captcha_tab_exist = setInterval(function () {
                            if (result_page_ready.hasOwnProperty(cap_tab.id)) {
                                try {
                                    chrome.tabs.executeScript(cap_tab.id, {code: 'window.onunload = function(e){chrome.runtime.sendMessage({captcha:"done",from_tab:' + tab.id + ',captcha_tab:' + cap_tab.id + '});}'});
                                    clearInterval(captcha_tab_exist);
                                } catch (e) {
                                }
                            }
                        }, 100);
                    });
            }
            return;
        }
        xhr_result = this.responseText;
        requested = true;
    };
    var request_ended = setInterval(function () {
        if (requested) {
            clearInterval(request_ended);
            if (step < 6) {
                navigation_hash = '';
                parseGooglePages(imageUrl, tab);
            }
            else {
                end_parse(imageUrl, tab);
            }
        }
    }, 500);
}
function end_parse(imageUrl, tab) {
    chrome.tabs.sendMessage(tab.id, {progressBar: "add", set: 100});
    console.table(answer);
    chrome.tabs.create({  //открываем наш сайт и передаём туда данные который заполняют страничку в context.js
            url: 'http://alipictures.ru/',
            active: true
        },
        function (new_tab) {
            var res_tab = new_tab.id;
            result_page[res_tab] = true;
            parseAliexpress(answer, imageUrl, new_tab);
            turn = false;
            step = 0;
            answer = {query:'',main: [], similar: {urls: [], results: []}};

            var set_interval = setInterval(function () {
                if (result_page_ready.hasOwnProperty(res_tab)) {
                        chrome.tabs.get(tab.id, function () {
                            if (chrome.runtime.lastError == null) {
                                chrome.tabs.executeScript(res_tab, {code: 'window.onunload = function(e){chrome.runtime.sendMessage({checkClosed:"go",check_this:' + res_tab + ',from_tab:' + tab.id + '});};'});
                                chrome.tabs.executeScript(res_tab, {code: '$("body").append("<img src=\'http://alipictures.ru/img/loading.gif\' id=\'loader-gif\'>")'}); //просто гифка с анимацией ожидания
                                clearInterval(set_interval);
                            }
                        });
                }
            },100);
        });
}

function parseAliexpress(res, imageUrl, tab) {
    var request = {"user_api_key":"b835a474ea86ff3d8ccb0af8db8ae479","user_hash":"pynb1ff1is0j1ifhmtpmn67vzk5gsdji","api_version":"2"};
    request['requests']={};
    for (var i = 0; i < res.main.length; i++) {
        request['requests']['main_'+i] = {};
        request['requests']['main_'+i]['action']='offer_info';
        request['requests']['main_'+i]['id']=res.main[i];
    }
    for (var i = 0; i < res.similar.results.length; i++) {
        request['requests']['similar_'+i] = {};
        request['requests']['similar_'+i]['action']='offer_info';
        request['requests']['similar_'+i]['id']=res.similar.results[i];
    }
    var xhr_ali = new XMLHttpRequest();
    xhr_ali.onreadystatechange = function () {
        if (this.readyState != 4) {
            return;
        }
        if (this.status != 200) {
            return;
        }
        chrome.tabs.executeScript(tab.id, {code: '$("#loader-gif").remove();'});
        var results = JSON.parse(this.responseText).results;
        for (var result in results){
            if (results.hasOwnProperty(result)){
                if (results[result].hasOwnProperty('offer')){
                    var price_old = '';
                    var price_now = '';
                    if (results[result]['offer'].hasOwnProperty('sale_price')){
                        price_now = '$'+results[result]['offer']['sale_price']+' USD';
                        price_old = '$'+results[result]['offer']['price']+' USD';
                    } else {
                        price_now = '$'+results[result]['offer']['price']+' USD';
                        price_old = 'no_promo';
                    }
                    if (price_now == price_old){
                        price_old = 'no_promo';
                    }
                    var item = {
                        query: results[result]['offer']['name']+' '+res.query,
                        url: 'https://aliexpress.com/item/'+results[result]['offer']['id']+'.html',
                        images: [results[result]['offer']['picture']],
                        price_old: price_old,
                        price_now: price_now
                    };
                    sendAliRes(item, tab, result);
                }
            }
        }
    };
    xhr_ali.open("POST", 'http://api.epn.bz/json', true);
    xhr_ali.send(JSON.stringify(request));
}
function sendAliRes(item, tab, turn){
    var interval = setInterval(function(){
        if (result_page_ready.hasOwnProperty(tab.id)) {
                chrome.tabs.get(tab.id, function () {
                    if (chrome.runtime.lastError == null) {
                        chrome.tabs.sendMessage(tab.id, {resultForTab: tab.id, result: item, turn: turn});
                        clearInterval(interval);
                    }
                });
        }
    },100);
}